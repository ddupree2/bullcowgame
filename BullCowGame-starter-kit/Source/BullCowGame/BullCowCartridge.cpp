// Fill out your copyright notice in the Description page of Project Settings.
#include "BullCowCartridge.h"
#include "HiddenWordList.h"

void UBullCowCartridge::BeginPlay() // When the game starts
{
    Super::BeginPlay();

    Isograms = GetValidWords(Words);
    SetupGame();

    //PrintLine(TEXT("%s"), *HiddenWord); //Debug line for testing purposes.
}

void UBullCowCartridge::OnInput(const FString& Input) // When the player hits enter
{
    PrintLine(Input);
    ProcessGuess(Input);
}

void UBullCowCartridge::SetupGame()
{
    HiddenWord = Isograms[FMath::RandRange(0, Isograms.Num() - 1)];
    Lives = HiddenWord.Len();
    bGameOver = false;

    PrintLine(TEXT("Welcome to Bull Cows."));
    PrintLine(TEXT("Guess the %i letter word."), HiddenWord.Len());
    PrintLine(TEXT("You have %i lives!"), Lives);
    PrintLine(TEXT("Type your guess and please \npress enter!"));
}

TArray<FString> UBullCowCartridge::GetValidWords(const TArray<FString>& WordList) const
{
    TArray<FString> ValidWords;
    for (FString Word : WordList)
   {
       if (Word.Len() >= 4 && Word.Len() <= 8 && IsIsogram(Word))
       {
          ValidWords.Emplace(Word);
       }
   }

   return ValidWords;
}

void UBullCowCartridge::EndGame()
{
    bGameOver = true;
    PrintLine(TEXT("The hidden word was: %s"), *HiddenWord);
    PrintLine(TEXT("\nPlease press Enter to play again."));
}

void UBullCowCartridge::CheckLives(const FString& Guess)
{
    --Lives;

    if(Lives <= 0){
        ClearScreen();
        PrintLine(TEXT("You have no more lives game over"));
        EndGame();
    }
    else
    {
        FBullCowCount Score = GetBullCows(Guess);
        PrintLine(TEXT("You have %i Bulls and %i Cows"), Score.Bulls, Score.Cows);

        PrintLine(TEXT("You have %i lives remaining."), Lives);
    }
}

bool UBullCowCartridge::IsIsogram(const FString& Word) const 
{

    for(int32 Index = 0; Index < Word.Len(); Index++)
    {
        for (int32 Comparison = Index +1; Comparison < Word.Len() - 1; Comparison++)
        {
            if (Word[Index] == Word[Comparison])
            {
                return false;
            } 
        }
    }
    
    return true;   
}

void UBullCowCartridge::ProcessGuess(const FString& Guess)
{
    if(bGameOver)
    {
        ClearScreen();
        SetupGame();
        return;
    }

    if(Guess == HiddenWord)
    {
        ClearScreen();
        PrintLine(TEXT("That is correct you win."));
        EndGame();
        return;
    }

    if(Guess.Len() != HiddenWord.Len())
    {
        PrintLine(TEXT("The hidden word is %i characters."), HiddenWord.Len());
        return;
    }

    if(!IsIsogram(Guess))
    {
        PrintLine(TEXT("No repeating letters, guess again."));
        return;
    }
         
    PrintLine(TEXT("That is incorrect."));   
    CheckLives(Guess);    
}

FBullCowCount UBullCowCartridge::GetBullCows(const FString& Guess) const{
    FBullCowCount Count;

    for (int32 GuessIndex = 0; GuessIndex < Guess.Len(); GuessIndex++)
    {
        if (Guess[GuessIndex] == HiddenWord[GuessIndex])
        {
            Count.Bulls++;
            continue;
        }

        for (int32 HiddenIndex = 0; HiddenIndex < HiddenWord.Len(); HiddenIndex++)
        {
            if (Guess[GuessIndex] == HiddenWord[HiddenIndex])
            {
                Count.Cows++;
                break;
            }
        }      
    }    
    return Count;
}